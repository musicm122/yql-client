﻿using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;
using System.Threading.Tasks;

namespace Yql_Client_Tests
{
    [TestClass]
    public class YqlApi_Tests
    {
        [TestMethod]
        public void TestMethod1()
        {
            Assert.AreEqual(0, 0);
        }

        [TestMethod]
        public async Task Valid_Query_Returns_value()
        {
            string sampleQuery = @"use 'http://yqlblog.net/samples/search.imageweb.xml' as searchimageweb; select * from searchimageweb where query='pizza'";
            var result = await YqlApi.QueryRequest.YQLQueryString(sampleQuery);

            Assert.IsNotNull(result);
            Assert.IsFalse(result == String.Empty);
        }
    }
}