﻿// ***********************************************************************
// Assembly         : YQL_Client
// Author           : terrancesmith
// Created          : 09-12-2012
//
// Last Modified By : terrancesmith
// Last Modified On : 11-04-2012
// ***********************************************************************
// <copyright file="MainViewModel.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.IO;
using WinRTXamlToolkit.IO.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using YqlApi;
using Windows.Storage;
using YQL_Client.Common;
using System.Collections.ObjectModel;



namespace Yql_Client_Logic.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : Common.BindableBase
    {
        /// <summary>
        /// The path
        /// </summary>
        string path = @"C:\Users\Terrance\Desktop\output.html";
        /// <summary>
        /// The default_ query
        /// </summary>
        const string Default_Query = @"use 'http://yqlblog.net/samples/search.imageweb.xml' as searchimageweb; select * from searchimageweb where query='pizza'";
        /// <summary>
        /// The m_db_helper
        /// </summary>
        SqliteHelper m_db_helper = null;

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
           Init();
            // this.InputQuery = "select * from html where url='http://www.craigslist.org/about/best/sfo/2549849730.html'";
            //if (IsInDesignMode)
            //{
            //    this.InputQuery = "select * from html where url='http://www.craigslist.org/about/best/sfo/2549849730.html'";
            //}
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
        }

        /// <summary>
        /// Inits this instance.
        /// </summary>
        private void Init()
        {
            m_db_helper = new SqliteHelper();
        }

        /// <summary>
        /// Loads the view model.
        /// </summary>
        void LoadViewModel()
        {
            this.BookmarkCollection = m_db_helper.GetBookmarks().ToObservableCollection();
            this.QueryHistory = m_db_helper.GetHistory().ToObservableCollection();
        }

        /// <summary>
        /// The <see cref="InputQuery" /> property's name.
        /// </summary>
        private string _inputQuery = string.Empty;
        /// <summary>
        /// Gets or sets the input query.
        /// </summary>
        /// <value>The input query.</value>
        public string InputQuery
        {
            get { return this._inputQuery; }
            set { this.SetProperty(ref this._inputQuery, value); }
        }

        /// <summary>
        /// The _ query status
        /// </summary>
        private string _QueryStatus = string.Empty;
        /// <summary>
        /// Gets or sets the query status.
        /// </summary>
        /// <value>The query status.</value>
        public string QueryStatus
        {
            get { return this._QueryStatus; }
            set { this.SetProperty(ref this._QueryStatus, value); }
        }

        /// <summary>
        /// The _ output text
        /// </summary>
        private string _OutputText = string.Empty;
        /// <summary>
        /// Gets or sets the output text.
        /// </summary>
        /// <value>The output text.</value>
        public string OutputText
        {
            get { return this._QueryStatus; }
            set { this.SetProperty(ref this._QueryStatus, value); }
        }

        /// <summary>
        /// The _ reusable URL
        /// </summary>
        private string _ReusableUrl = string.Empty;
        /// <summary>
        /// Gets or sets the reusable URL.
        /// </summary>
        /// <value>The reusable URL.</value>
        public string ReusableUrl 
        {
            get { return this._ReusableUrl; }
            set { this.SetProperty(ref _ReusableUrl, value);}
        }

        /// <summary>
        /// The _ query history
        /// </summary>
        private ObservableCollection<HistoryItem> _QueryHistory = new ObservableCollection<HistoryItem>();
        /// <summary>
        /// Gets or sets the query history.
        /// </summary>
        /// <value>The query history.</value>
        public ObservableCollection<HistoryItem> QueryHistory
        {
            get
            {
                return this._QueryHistory;
            }
            set
            {
                this.SetProperty(ref _QueryHistory, value);
            }
        }

        /// <summary>
        /// The _ bookmark collection
        /// </summary>
        private ObservableCollection<BookmarkItem> _BookmarkCollection =new ObservableCollection<BookmarkItem>();
        /// <summary>
        /// Gets or sets the bookmark collection.
        /// </summary>
        /// <value>The bookmark collection.</value>
        public ObservableCollection<BookmarkItem> BookmarkCollection
        {
            get
            {
                return _BookmarkCollection;
            }

            set
            {
                SetProperty(ref _BookmarkCollection, value);
            }
        }


        /// <summary>
        /// The _ execute query command
        /// </summary>
        private DelegateCommand _ExecuteQueryCommand;

        /// <summary>
        /// Gets the ExecuteQueryCommand.
        /// </summary>
        /// <value>The execute query command.</value>
        public DelegateCommand ExecuteQueryCommand
        {
            get
            {
                return _ExecuteQueryCommand ?? (_ExecuteQueryCommand = new DelegateCommand(
                    ExecuteQuery
                    ));
            }
        }

        /// <summary>
        /// Executes the query.
        /// </summary>
        /// <param name="obj">The obj.</param>
        private async void ExecuteQuery(object obj)
        {
            try
            {
                //var result = await YqlApi.QueryRequest.YQLQuery(this.InputQuery);
                var result = await YqlApi.QueryRequest.YQLQueryHttpResponse(this.InputQuery);
                
                this.OutputText = await result.Content.ReadAsStringAsync();
                SaveToHistoryFile(this.OutputText);
                
                if (result.IsSuccessStatusCode)
                {
                    QueryStatus = "Query Executed Successfully";
                }
                else 
                {
                    QueryStatus = result.StatusCode.ToString() + " " + result.ReasonPhrase;
                }
                ReusableUrl = YqlApi.QueryRequest.FormatRequestUrl(this.InputQuery);
                AddHistory();
            }
            catch (Exception e)
            {
                QueryStatus = e.ToString();
                this.OutputText = e.ToString();
            }
        }

        /// <summary>
        /// Determines whether this instance [can execute query] the specified obj.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns><c>true</c> if this instance [can execute query] the specified obj; otherwise, <c>false</c>.</returns>
        private bool CanExecuteQuery(object obj)
        {
            if (!String.IsNullOrEmpty(InputQuery))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Adds the bookmark.
        /// </summary>
        private void AddBookmark() 
        {
            var new_item = new BookmarkItem() { Url = this.ReusableUrl, Query = this.InputQuery };
            this.BookmarkCollection.Add(new_item);
            m_db_helper.AddBookmarkItem(new_item);
        }

        /// <summary>
        /// Adds the history.
        /// </summary>
        private void AddHistory() 
        {
            var new_item = new HistoryItem() {
                Url = this.ReusableUrl,
                Query = this.InputQuery, 
                DtStamp = DateTime.Now
            };
            this.QueryHistory.Add(new_item);
            m_db_helper.AddHistoryItem(new_item);
        }

        /// <summary>
        /// Gets the state of the page.
        /// </summary>
        private void GetPageState()
        {
        }

        /// <summary>
        /// Saves to history file.
        /// </summary>
        /// <param name="val">The val.</param>
        private async void SaveToHistoryFile(HistoryItem val)
        {
            var LocalFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            StorageFile myFile = await LocalFolder.CreateFileAsync("history.txt", CreationCollisionOption.OpenIfExists);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(
                val.Id+","+
                val.DtStamp.ToString()+","+
                val.Query+","+
                val.Url
            );
            await FileIO.WriteTextAsync(myFile, sb.ToString());
        }

        /// <summary>
        /// Saves to history file.
        /// </summary>
        /// <param name="val">The val.</param>
        private async void SaveToHistoryFile(string val)
        {
            var LocalFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            StorageFile myFile = await LocalFolder.CreateFileAsync("history.txt", CreationCollisionOption.OpenIfExists);
            await FileIO.WriteTextAsync(myFile, val + "\n");
        }

        /// <summary>
        /// Reterives from history file.
        /// </summary>
        private async void RetriveFromHistoryFile() 
        {
            _QueryHistory = _QueryHistory ?? new ObservableCollection<HistoryItem>();
            
            StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync("history.txt");
            var result = await StringIOExtensions.ReadFromFile(file.DisplayName);
            var historyItems = result.Split('\n');
            foreach (var item in historyItems)
	        {
                var historyItem = result.Split(',');
                _QueryHistory.Add(new HistoryItem(){
                    Id = Convert.ToInt32(historyItem[0]),
                    DtStamp = DateTime.Parse(historyItems[1]),
                    Query = historyItems[2],
                    Url = historyItems[3]
                });
	        }
        }
    }
}
