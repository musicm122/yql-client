﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using YQL_Client.Enums;

namespace YQL_Client.Converters
{
    public class EnumVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var splashState = (VisualPageState)value;
            string currentVisibleState = (string)parameter;

            if (currentVisibleState == Enum.GetName(typeof(VisualPageState), splashState))
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}