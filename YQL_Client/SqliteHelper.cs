﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using YQL_Client.ViewModel;

namespace YQL_Client
{
    public class SqliteHelper
    {
        public static string dbPath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "db.sqlite");

        public SqliteHelper()
        {
            CreateDb();
        }

        public bool AddBookmarkItem(BookmarkItem item)
        {
            try
            {
                using (var db = new SQLite.SQLiteConnection(dbPath))
                {
                    db.RunInTransaction(() =>
                    {
                        Debug.WriteLine("inserting entry into bookmark table {0},{1},{2},{3}", item.Id.ToString(), item.Query, item.Url, item.DtStamp.ToString());
                        db.Insert(item);
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return false;
            }
            return true;
        }

        public bool AddHistoryItem(HistoryItem item)
        {
            try
            {
                using (var db = new SQLite.SQLiteConnection(dbPath))
                {
                    db.RunInTransaction(() =>
                    {
                        Debug.WriteLine("inserting entry into history table {0},{1},{2},{3}", item.Id.ToString(), item.Query, item.Url, item.DtStamp.ToString());
                        db.Insert(item);
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return false;
            }
            return true;
        }

        public BookmarkItem GetBookmarkItem(int id)
        {
            BookmarkItem retval;
            using (var db = new SQLite.SQLiteConnection(dbPath))
            {
                retval = db.Get<BookmarkItem>(id);
            }
            return retval;
        }

        public IEnumerable<BookmarkItem> GetBookmarks()
        {
            TableQuery<BookmarkItem> retval;
            using (var db = new SQLite.SQLiteConnection(dbPath))
            {
                retval = db.Table<BookmarkItem>();
            }
            return retval;
        }

        public IEnumerable<HistoryItem> GetHistory()
        {
            TableQuery<HistoryItem> retval;
            using (var db = new SQLite.SQLiteConnection(dbPath))
            {
                retval = db.Table<HistoryItem>();
            }
            return retval;
        }

        public HistoryItem GetHistoryItem(int id)
        {
            HistoryItem retval;
            using (var db = new SQLite.SQLiteConnection(dbPath))
            {
                retval = db.Get<HistoryItem>(id);
            }
            return retval;
        }

        private void CreateDb()
        {
            using (var db = new SQLite.SQLiteConnection(dbPath))
            {
                db.CreateTable<HistoryItem>();
                db.CreateTable<BookmarkItem>();
            }
        }
    }
}