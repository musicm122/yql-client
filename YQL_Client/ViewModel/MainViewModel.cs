﻿// ***********************************************************************
// Assembly         : YQL_Client
// Author           : terrancesmith
//
// ***********************************************************************
// <copyright file="MainViewModel.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.ObjectModel;
using System.Text;
using Windows.Storage;
using WinRTXamlToolkit.IO.Extensions;
using YQL_Client.Common;
using YQL_Client.Enums;

namespace YQL_Client.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm[
    /// </para>
    /// </summary>
    public class MainViewModel : Common.BindableBase
    {
        public bool _IsPageContentVisible;

        /// <summary>
        /// The default_ query
        /// </summary>
        private const string Default_Query = @"use 'http://yqlblog.net/samples/search.imageweb.xml' as searchimageweb; select * from searchimageweb where query='pizza'";

        /// <summary>
        /// The _ execute query command
        /// </summary>
        private DelegateCommand _AddBookmarkCommand;

        /// <summary>
        /// The _ bookmark collection
        /// </summary>
        private ObservableCollection<BookmarkItem> _BookmarkCollection = new ObservableCollection<BookmarkItem>();

        private VisualPageState _CurrentPageState = VisualPageState.Empty;

        /// <summary>
        /// The _ execute query command
        /// </summary>
        private DelegateCommand _ExecuteQueryCommand;

        /// <summary>
        /// The <see cref="InputQuery" /> property's name.
        /// </summary>
        private string _inputQuery = string.Empty;

        /// <summary>
        /// The _ output text
        /// </summary>
        private string _OutputText = string.Empty;

        /// <summary>
        /// The _ query history
        /// </summary>
        private ObservableCollection<HistoryItem> _QueryHistory = new ObservableCollection<HistoryItem>();

        /// <summary>
        /// The _ query status
        /// </summary>
        private string _QueryStatus = string.Empty;

        /// <summary>
        /// The _ reusable URL
        /// </summary>
        private string _ReusableUrl = string.Empty;

        private DelegateCommand _SelectTabCommand;

        /// <summary>
        /// The m_db_helper
        /// </summary>
        private SqliteHelper m_db_helper = null;

        /// <summary>
        /// The path
        /// </summary>
        private string path = @"C:\Users\Terrance\Desktop\output.html";

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            Init();
#if DEBUG
            this.InputQuery = "select * from html where url='http://www.craigslist.org/about/best/sfo/2549849730.html'";
#endif

            //if (IsInDesignMode)
            //{
            //    this.InputQuery = "select * from html where url='http://www.craigslist.org/about/best/sfo/2549849730.html'";
            //}
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
        }

        /// <summary>
        /// Gets the ExecuteQueryCommand.
        /// </summary>
        /// <value>The execute query command.</value>
        public DelegateCommand AddBookmarkCommand
        {
            get
            {
                return _AddBookmarkCommand ?? (_AddBookmarkCommand = new DelegateCommand(
                    AddBookmark,
                    CanAddBookmark
                    ));
            }
        }

        /// <summary>
        /// Gets or sets the bookmark collection.
        /// </summary>
        /// <value>The bookmark collection.</value>
        public ObservableCollection<BookmarkItem> BookmarkCollection
        {
            get
            {
                return _BookmarkCollection;
            }

            set
            {
                SetProperty(ref _BookmarkCollection, value);
            }
        }

        public VisualPageState CurrentPageState
        {
            get { return this._CurrentPageState; }
            set { this.SetProperty(ref _CurrentPageState, value); }
        }

        /// <summary>
        /// Gets the ExecuteQueryCommand.
        /// </summary>
        /// <value>The execute query command.</value>
        public DelegateCommand ExecuteQueryCommand
        {
            get
            {
                return _ExecuteQueryCommand ?? (_ExecuteQueryCommand = new DelegateCommand(
                    ExecuteQuery
                    ));
            }
        }

        /// <summary>
        /// Gets or sets the input query.
        /// </summary>
        /// <value>The input query.</value>
        public string InputQuery
        {
            get { return this._inputQuery; }
            set { this.SetProperty(ref this._inputQuery, value); }
        }

        public bool IsPageContentVisible
        {
            get
            {
                return this._IsPageContentVisible;
            }
            set
            {
                this.SetProperty(ref _IsPageContentVisible, value);
            }
        }

        /// <summary>
        /// Gets or sets the output text.
        /// </summary>
        /// <value>The output text.</value>
        public string OutputText
        {
            get
            {
                return this._QueryStatus;
            }
            set
            {
                this.SetProperty(ref this._QueryStatus, value);
                if (!String.IsNullOrWhiteSpace(value))
                {
                    IsPageContentVisible = true;
                }
                else
                {
                    IsPageContentVisible = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets the query history.
        /// </summary>
        /// <value>The query history.</value>
        public ObservableCollection<HistoryItem> QueryHistory
        {
            get
            {
                return this._QueryHistory;
            }
            set
            {
                this.SetProperty(ref _QueryHistory, value);
            }
        }

        /// <summary>
        /// Gets or sets the query status.
        /// </summary>
        /// <value>The query status.</value>
        public string QueryStatus
        {
            get { return this._QueryStatus; }
            set { this.SetProperty(ref this._QueryStatus, value); }
        }

        /// <summary>
        /// Gets or sets the reusable URL.
        /// </summary>
        /// <value>The reusable URL.</value>
        public string ReusableUrl
        {
            get { return this._ReusableUrl; }
            set { this.SetProperty(ref _ReusableUrl, value); }
        }

        /// <summary>
        /// Gets the ExecuteQueryCommand.
        /// </summary>
        /// <value>The execute query command.</value>
        public DelegateCommand SelectTabCommand
        {
            get
            {
                return _SelectTabCommand ?? (_SelectTabCommand = new DelegateCommand(
                    ChangeTab, CanChangeTab
                    ));
            }
        }

        public bool CanChangeTab(object obj)
        {
            return !(String.IsNullOrWhiteSpace(this.OutputText));
        }

        public void ChangeTab(object obj)
        {
            var selectedVis = (VisualPageState)obj;
            this.CurrentPageState = selectedVis;
        }

        /// <summary>
        /// Adds the bookmark.
        /// </summary>
        private void AddBookmark(object obj)
        {
            var new_item = new BookmarkItem() { Url = this.ReusableUrl, Query = this.InputQuery };
            this.BookmarkCollection.Add(new_item);
            m_db_helper.AddBookmarkItem(new_item);
        }

        /// <summary>
        /// Adds the history.
        /// </summary>
        private void AddHistory()
        {
            var new_item = new HistoryItem()
            {
                Url = this.ReusableUrl,
                Query = this.InputQuery,
                DtStamp = DateTime.Now
            };
            this.QueryHistory.Add(new_item);
            m_db_helper.AddHistoryItem(new_item);
        }

        private bool CanAddBookmark(object obj)
        {
            return (!string.IsNullOrWhiteSpace(this._ReusableUrl) && !string.IsNullOrWhiteSpace(this.InputQuery));
        }

        /// <summary>
        /// Determines whether this instance [can execute query] the specified obj.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns><c>true</c> if this instance [can execute query] the specified obj; otherwise, <c>false</c>.</returns>
        private bool CanExecuteQuery(object obj)
        {
            if (!String.IsNullOrEmpty(InputQuery))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Executes the query.
        /// </summary>
        /// <param name="obj">The obj.</param>
        private async void ExecuteQuery(object obj)
        {
            try
            {
                //var result = await YqlApi.QueryRequest.YQLQuery(this.InputQuery);
                var result = await YqlApi.QueryRequest.YQLQueryHttpResponse(this.InputQuery);

                this.OutputText = await result.Content.ReadAsStringAsync();
                SaveToHistoryFile(this.OutputText);

                if (result.IsSuccessStatusCode)
                {
                    QueryStatus = "Query Executed Successfully";
                }
                else
                {
                    QueryStatus = result.StatusCode.ToString() + " " + result.ReasonPhrase;
                }
                ReusableUrl = YqlApi.QueryRequest.FormatRequestUrl(this.InputQuery);
                AddHistory();
            }
            catch (Exception e)
            {
                QueryStatus = e.ToString();
                this.OutputText = e.ToString();
            }
        }

        /// <summary>
        /// Gets the state of the page.
        /// </summary>
        private void GetPageState()
        {
        }

        /// <summary>
        /// Inits this instance.
        /// </summary>
        private void Init()
        {
            m_db_helper = new SqliteHelper();
        }

        /// <summary>
        /// Loads the view model.
        /// </summary>
        private void LoadViewModel()
        {
            this.BookmarkCollection = m_db_helper.GetBookmarks().ToObservableCollection();
            this.QueryHistory = m_db_helper.GetHistory().ToObservableCollection();
        }

        /// <summary>
        /// Reterives from history file.
        /// </summary>
        private async void RetriveFromHistoryFile()
        {
            _QueryHistory = _QueryHistory ?? new ObservableCollection<HistoryItem>();

            StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync("history.txt");
            var result = await StringIOExtensions.ReadFromFile(file.DisplayName);
            var historyItems = result.Split('\n');
            foreach (var item in historyItems)
            {
                var historyItem = result.Split(',');
                _QueryHistory.Add(new HistoryItem()
                {
                    Id = Convert.ToInt32(historyItem[0]),
                    DtStamp = DateTime.Parse(historyItems[1]),
                    Query = historyItems[2],
                    Url = historyItems[3]
                });
            }
        }

        /// <summary>
        /// Saves to history file.
        /// </summary>
        /// <param name="val">The val.</param>
        private async void SaveToHistoryFile(HistoryItem val)
        {
            var LocalFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            StorageFile myFile = await LocalFolder.CreateFileAsync("history.txt", CreationCollisionOption.OpenIfExists);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(
                val.Id + "," +
                val.DtStamp.ToString() + "," +
                val.Query + "," +
                val.Url
            );
            await FileIO.WriteTextAsync(myFile, sb.ToString());
        }

        /// <summary>
        /// Saves to history file.
        /// </summary>
        /// <param name="val">The val.</param>
        private async void SaveToHistoryFile(string val)
        {
            var LocalFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            StorageFile myFile = await LocalFolder.CreateFileAsync("history.txt", CreationCollisionOption.OpenIfExists);
            await FileIO.WriteTextAsync(myFile, val + "\n");
        }

        private void ViewBookmarks()
        {
            var new_item = new BookmarkItem() { Url = this.ReusableUrl, Query = this.InputQuery };
            this.BookmarkCollection.Add(new_item);
            m_db_helper.AddBookmarkItem(new_item);
        }
    }
}