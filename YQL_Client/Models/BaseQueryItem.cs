﻿using SQLite;
using System;

namespace YQL_Client.Models
{
    public abstract class BaseQueryItem
    {
        public String DisplayDateTime
        {
            get { return DtStamp.ToString(); }
        }

        public DateTime DtStamp { get; set; }

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Query { get; set; }

        public string Url { get; set; }
    }
}