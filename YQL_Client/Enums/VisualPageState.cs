﻿namespace YQL_Client.Enums
{
    public enum VisualPageState
    {
        Text, Html, Web, Empty
    }
}