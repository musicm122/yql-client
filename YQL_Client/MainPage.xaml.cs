﻿using Callisto.Controls;
using System;
using System.Collections.Generic;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Navigation;
using YQL_Client.Common;
using YQL_Client.Extensions;
using YQL_Client.ViewModel;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace YQL_Client
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : LayoutAwarePage
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        public void Load_Html(object sender, RoutedEventArgs e)
        {
            WebOutput.NavigateToString("");

            WebOutput.NavigateToString(TextOutput.Text);
        }

        public void LoadInit()
        {
            WebOutput.NavigateToString("Web Output is rendered as html here");
            TextOutput.Text = "Html Output is displayed here";
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private static void OpenFlyoutHyperlink(object sender, string url)
        {
            Flyout f = new Flyout();

            // creating some content
            Border b = new Border();
            b.Width = 300;
            b.Height = 125;

            HyperlinkButton btn = new HyperlinkButton();
            btn.NavigateUri = new Uri(url, UriKind.Absolute);
            btn.Content = url;
            btn.FontSize = 25;

            b.Child = btn;

            // set the Flyout content
            f.Content = b;

            // set the placement
            f.Placement = PlacementMode.Top;
            f.PlacementTarget = sender as UIElement;

            // open the flyout
            f.IsOpen = true;
        }

        private void AboutBtn_Click_1(object sender, RoutedEventArgs e)
        {
            var result = Launcher.LaunchUriAsync(new Uri("http://musicm122.blogspot.com/"));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
        }

        private StackPanel FormattedElement(BookmarkItem item)
        {
            var retval = new StackPanel();
            retval.Orientation = Orientation.Horizontal;
            retval.Children.Add(new TextBlock() { Text = "Query:" });
            retval.Children.Add(new TextBox() { Text = item.Query });

            retval.Children.Add(new TextBlock() { Text = "Url:" });
            retval.Children.Add(new TextBox() { Text = item.Url });
            return retval;
        }

        private void ShowFavoritesFlyout(object sender, RoutedEventArgs e, IEnumerable<BookmarkItem> items)
        {
            Popup f = new Popup();

            // creating some content
            // this could be just an instance of a UserControl of course
            Border b = new Border();

            b.Width = Window.Current.Bounds.Width - 20;
            b.Height = Window.Current.Bounds.Height - 20;
            StackPanel sp = new StackPanel();
            sp.Orientation = Orientation.Vertical;
            b.Child = sp;
            var innerChildSp = new StackPanel();
            foreach (var item in items)
            {
                sp.Children.Add(FormattedElement(item));
            }

            TextBox tb = new TextBox();
            tb.IsReadOnly = true;

            tb.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            tb.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            tb.TextWrapping = TextWrapping.Wrap;
            tb.FontSize = 24;

            tb.Text = items.ToFormattedOutputString();

            b.Child = tb;

            // set the Flyout content

            f.Child = b;
            f.IsLightDismissEnabled = true;

            // set the placement
            //f.Placement = PlacementMode.Left;

            //f.PlacementTarget = sender as UIElement;

            // open the flyout
            f.IsOpen = true;
        }

        private void TabControl_SelectionChanged_1(object sender, Windows.UI.Xaml.Controls.SelectionChangedEventArgs e)
        {
        }

        private void ViewFavorites_Click_1(object sender, RoutedEventArgs e)
        {
            ViewModelLocator vml = new ViewModel.ViewModelLocator();
            ShowFavoritesFlyout(sender, e, vml.Main.BookmarkCollection);
        }

        private void XpathHelpBtn_Click_1(object sender, RoutedEventArgs e)
        {
            var result = Launcher.LaunchUriAsync(new Uri("http://msdn.microsoft.com/en-us/library/ms256086.aspx"));

            //OpenFlyoutHyperlink(sender, "http://msdn.microsoft.com/en-us/library/ms256086.aspx");
        }

        private void YqlHelpBtn_Click_1(object sender, RoutedEventArgs e)
        {
            var result = Launcher.LaunchUriAsync(new Uri("http://developer.yahoo.com/yql/"));
        }
    }
}