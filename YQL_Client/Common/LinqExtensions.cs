﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using YQL_Client.ViewModel;

namespace YQL_Client.Common
{
    public static class LinqExtensions
    {
        public static ObservableCollection<HistoryItem> ToObservableCollection(this IEnumerable<HistoryItem> enumerable)
        {
            var col = new ObservableCollection<HistoryItem>();
            foreach (var cur in enumerable)
            {
                col.Add(cur);
            }
            return col;
        }

        public static ObservableCollection<BookmarkItem> ToObservableCollection(this IEnumerable<BookmarkItem> enumerable)
        {
            var col = new ObservableCollection<BookmarkItem>();
            foreach (var cur in enumerable)
            {
                col.Add(cur);
            }
            return col;
        }
    }
}