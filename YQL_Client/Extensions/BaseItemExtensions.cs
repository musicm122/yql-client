﻿using System;
using System.Collections.Generic;
using System.Text;
using YQL_Client.Models;
using YQL_Client.ViewModel;

namespace YQL_Client.Extensions
{
    public static class BaseItemExtensions
    {
        public static string ToFormattedOutputString(this IEnumerable<BaseQueryItem> items)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in items)
            {
                sb.AppendLine(String.Format("Query = {0}: Url = {1}\r\n", item.Query, item.Url));
            }

            return sb.ToString();
        }

        public static string ToFormattedOutputString(this IEnumerable<BookmarkItem> items)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in items)
            {
                sb.AppendLine(String.Format("Query = {0}: Url = {1}\r\n", item.Query, item.Url));
            }

            return sb.ToString();
        }

        public static string ToFormattedOutputString(this IEnumerable<HistoryItem> items)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in items)
            {
                sb.AppendLine(String.Format("Query = {0}: Url = {1}\r\n", item.Query, item.Url));
            }

            return sb.ToString();
        }
    }
}