﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Navigation;
using YQL_Client.Common;
using YQL_Client.ViewModel;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace YQL_Client
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : LayoutAwarePage
    {
        public MainPage()
        {
            this.InitializeComponent();

            //WebOutput.NavigateToString("Web Output is rendered as html here");
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        public void Load_Html(object sender, RoutedEventArgs e)
        {
            //WebOutput.NavigateToString("");
            //WebOutput.NavigateToString(TextOutput.Text);
        }

        private void TabControl_SelectionChanged_1(object sender, Windows.UI.Xaml.Controls.SelectionChangedEventArgs e)
        {
           
        }
    }
}