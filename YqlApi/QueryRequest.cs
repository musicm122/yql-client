﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace YqlApi
{
    /// <summary>
    ///
    /// </summary>
    public static class QueryRequest
    {
        /// <summary>
        /// The base json URL
        /// </summary>
        public const string BaseJsonUrl = "http://query.yahooapis.com/v1/yql?format=json&q=";

        /// <summary>
        /// The base XML URL
        /// </summary>
        public const string BaseXmlUrl = "http://query.yahooapis.com/v1/yql?format=xml&q=";

        private const string YqlRequestUrl = "https://api.login.yahoo.com/oauth/v2/get_request_token";
        private static string baseUrl = "http://query.yahooapis.com/v1/public/yql?format=xml&diagnostics=true&q=";

        //select * from html where url='http://www.yahoo.com/'
        //http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url%3D'http%3A%2F%2Fwww.yahoo.com%2F'&diagnostics=true
        //http://query.yahooapis.com/v1/public/yql?q=''&diagnostics=true

        public static string FormatRequestUrl(string yql)
        {
            return baseUrl + Uri.EscapeUriString(yql);
        }

        /// <summary>
        /// YQLs the query HTTP response.
        /// </summary>
        /// <param name="yql">The yql.</param>
        /// <returns></returns>
        public async static Task<HttpResponseMessage> YQLQueryHttpResponse(string yql)
        {
            string url = FormatRequestUrl(yql);

            HttpResponseMessage response = new HttpResponseMessage();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    response = await client.GetAsync(url);
                }
                catch (System.Net.WebException)
                {
                    response = new HttpResponseMessage(System.Net.HttpStatusCode.NotFound);
                }
            }

            return response;
        }

        public async static Task<string> YQLQueryString(string yql)
        {
            string url = FormatRequestUrl(yql);
            string result = string.Empty;
            HttpResponseMessage response = new HttpResponseMessage();

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    response = await client.GetAsync(url);
                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        response.EnsureSuccessStatusCode();
                    }
                }
                catch (HttpRequestException e)
                {
                    result = e.ToString();
                }
            }

            return result;
        }
    }
}